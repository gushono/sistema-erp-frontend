const gulp = require('gulp');
const cssnano = require('cssnano');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');

const filesPath = {
    scss: 'app/scss/**/*.scss',
    js: 'app/js/**/*.js'
}

function scssTask() {
    return gulp.src(filesPath.scss)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'));
}

// Watch task
function watchTask() {
    // This task will be executed whenever a change occurs in the described paths
    gulp.watch([
        filesPath.scss,
        filesPath.js
    ], gulp.parallel(scssTask));
}

// Default task
exports.default = gulp.series(
    watchTask
);